import { NgModule } from '@angular/core';
import { NumberSliderComponent } from './number-slider';
import { CommonModule } from '@angular/common';
import { IonicModule } from "ionic-angular";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
	declarations: [NumberSliderComponent],
	imports: [CommonModule, IonicModule, FormsModule, ReactiveFormsModule],
	exports: [NumberSliderComponent]
})
export class NumberSliderModule { }
