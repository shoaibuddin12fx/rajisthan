import { GlobalProvider } from './../../providers/global/global';
import { Component, Input, Output, EventEmitter } from '@angular/core';
import { ThemeProvider } from '../../providers/theme/theme';

@Component({
  selector: 'number-slider',
  templateUrl: 'number-slider.html'
})
export class NumberSliderComponent {



  @Input('seed') number: number = 0;
  @Input('max') max: number = 100;
  @Input('min') min: number = 1;
  @Input('mock') mock: boolean = false;
  @Input('disabled') disabled: boolean = false;
  @Input('noneditable') noneditable: boolean = true;


  @Output('increased') increased: EventEmitter<number> = new EventEmitter<number>();
  @Output('decreased') decreased: EventEmitter<number> = new EventEmitter<number>();
  @Output('updateInput') updateInput: EventEmitter<number> = new EventEmitter<number>();
  

  constructor(
    public global: GlobalProvider,
    public theme: ThemeProvider,
  ) {
    console.log('Hello NumberSliderComponent Component');
  }

  decrease() {
    if(this.disabled == true) return;
    this.global.cLog('in decrease()', this.number, this.mock);

    if (!this.mock) {
      if (this.number > this.min) {
        this.number--;
        this.emitValueDecreased();
      } else {
        this.number = this.min;
      }
    }
  }

  increase() {
    if(this.disabled == true) return;
    this.global.cLog('in increase()', this.number, this.mock);

    if (!this.mock) {
      if (this.number < this.max) {
        this.number++;
        this.emitValueIncreased();
      } else {
        this.number = this.max;
      }
    }
  }

  emitValueChange($event) {

    if(this.disabled == true) return;
    var _value = $event.target.value
    if (!this.mock) {
      _value = _value.replace(/\D/g,'');
      if (_value < this.min){
        _value = this.min;
      }else if(_value > this.max){
        _value = this.max;
      }

      $event.target.value = _value;
      //this.number = _value
      this.updateInput.emit($event.target.value);
    }
    
  }

  emitValueIncreased() {
    this.increased.emit(this.number);
  }

  emitValueDecreased() {
    this.decreased.emit(this.number);
  }

}
