import { Directive, ElementRef, Renderer } from '@angular/core';
import { Platform } from 'ionic-angular/platform/platform';
import { Events } from 'ionic-angular';
import { Keyboard } from '@ionic-native/keyboard';

/**
 * Generated class for the ReactToKeyboardDirective directive.
 *
 * See https://angular.io/api/core/Directive for more info on Angular
 * Directives.
 */
@Directive({
  selector: '[react-to-keyboard]' // Attribute selector
})
export class ReactToKeyboardDirective {

  public showSub;
  public hideSub;
  private defaultHeight: number;
  private defaultPaddingTop: number;
  private defaultPaddingBottom: number;
  private keyboardIsShowing: boolean = false;

  constructor(public keyboard: Keyboard, public el: ElementRef, public renderer: Renderer, private platform: Platform, public events: Events) {
    if (platform.is('ios')) {
      this.defaultHeight = window.document.body.getBoundingClientRect().height;
      this.defaultPaddingTop = this.el.nativeElement.style.paddingTop;
      this.defaultPaddingBottom = this.el.nativeElement.style.paddingBottom;

      this.showSub = keyboard.onKeyboardShow().subscribe((a) => {
        if (this.keyboardIsShowing) {
          // Ensure iOS didn't resize the body element randomly... ><
          this.renderer.setElementStyle(this.el.nativeElement.parentElement, 'height', this.defaultHeight + 'px');
          return;
        }
        this.keyboardIsShowing = true;

        this.el.nativeElement.style.paddingBottom = (a.keyboardHeight + 20).toString() + 'px';
        this.el.nativeElement.style.paddingTop = '3rem';

        this.events.publish('react-to-keyboard:padding-added');
      });

      this.hideSub = keyboard.onKeyboardHide().subscribe((b) => {
        this.keyboardIsShowing = false;
        
        this.el.nativeElement.style.paddingTop = this.defaultPaddingTop;
        this.el.nativeElement.style.paddingBottom = this.defaultPaddingBottom;

        this.events.publish('react-to-keyboard:padding-removed');
      });
    }
  }

  ngOnDestroy() {
    if (this.showSub) {
      this.showSub.unsubscribe();
    }
    if (this.hideSub) {
      this.hideSub.unsubscribe();
    }
  }

}
