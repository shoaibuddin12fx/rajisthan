import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PalertPage } from './palert';

@NgModule({
  declarations: [
    PalertPage,
  ],
  imports: [
    IonicPageModule.forChild(PalertPage),
  ],
})
export class PalertPageModule {}
