import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ChoicesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-choices',
  templateUrl: 'choices.html',
})
export class ChoicesPage {

  cities: any;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams) {



      var data = this.navParams.get('data');
      console.log(data);



  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChoicesPage');
  }

}
