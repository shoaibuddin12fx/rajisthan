import { GlobalProvider } from './../../providers/global/global';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Events, Content,LoadingController } from 'ionic-angular';
import { Keyboard } from '@ionic-native/keyboard';
import { ThemeProvider } from '../../providers/theme/theme';
import { FormControl } from '@angular/forms';
//import {SuperTabsController } from 'ionic2-super-tabs';

@IonicPage()
@Component({
  selector: 'page-community-app-name',
  templateUrl: 'community-app-name.html',
})
export class CommunityAppNamePage {
  noData: boolean;
  userList: any;
  data:any;
  search: FormControl = new FormControl();
  searchType: any = 'Profession';
  @ViewChild(Content) content: Content;
  loader:any;
  userDetails:any;

  constructor(public navCtrl: NavController,public navParams: NavParams,public global: GlobalProvider,public event: Events,
    public keyboard: Keyboard,public theme: ThemeProvider,public events: Events,public loadingCtrl:LoadingController) 
  {
    //this.navCtrl.pop();
    //this.navCtrl.setRoot('CommunityAppNamePage');

    //alert(this.navCtrl.getPrevious().name);
    /*if(this.navCtrl.getPrevious().name == "Page2"){
      console.log("Remove Page2");
      this.navCtrl.remove(this.navCtrl.getPrevious().index);
    } */

    this.userDetails = JSON.parse(localStorage.getItem('user'));
    console.log("text");
    console.log(this.userDetails);
    console.log("end text");
    this.getSearchResultByDropdown(true);

    this.search.valueChanges.debounceTime(300).subscribe(
        res => {
          this.global.cLog(`in value change and the value is `, res);
          if (res && res.length) {
            this.showLoaders();
            this.global.postRequest(this.global.base_path + 'Login/SerachTextBox', { searchby: this.searchType, searchtext: res }).subscribe(
                res => {
                  this.hideLoaders();
                  this.global.cLog(`getSearchResult data`, res);
                  if (res.success == 'true')
                   {
                    this.userList =[];
                    this.data= res.data;
                    this.data.forEach((val, i) => 
                    {
                      if(val.value!=undefined && val.value!='undefined')
                      {
                       this.userList.push(val);
                      }
                    });    
                    if (this.userList && this.userList.length > 0) {
                      this.noData = false;
                    } else {
                      this.noData = true;
                    }
                  } else {
                    this.noData = true;
                    this.global.cLog(`getSearchResult error`, res);
                    //this.global.showToast(res.error);
                  }
                }, err => {
                  this.noData = true;
                  this.global.cLog(`getSearchResult error`, err);
                });
          } else {
            this.getSearchResultByDropdown(true);
          }
        });
  }

  checkPage()
  {
    this.events.subscribe('checkHome', data => { console.log(this.navCtrl.getActive().name);
      if(this.navCtrl.getActive().name=='ChatPage')
      {
        this.navCtrl.pop();
      }
    });
  }

  ionViewDidLoad() 
  {

    console.log('ionViewDidLoad CommunityAppNamePage');

    // this.keyboard.onKeyboardHide().subscribe(
    //   res => {
    //     this.global.cLog(`in onKeyboardHide`, res);
    //     this.removePadding();
    //   }, err => {
    //     this.removePadding();
    //   });

    // this.keyboard.onKeyboardShow().subscribe(
    //   res => {
    //     this.global.cLog(`in onKeyboardHide`, res);
    //     this.removePadding();
    //   }, err => {
    //     this.removePadding();
    //   });
  }

  ionViewDidEnter()
  {
  	this.userDetails = JSON.parse(localStorage.getItem('user'));
    console.log("text");
    console.log(this.userDetails);
    console.log("end text");
    this.checkPage();
  	this.getHome();
  }

  personDetails(person: any) {
    this.global.cLog(`in personDetails's method`, person);
  }

  makeCall(person: any) {
    if(this.userDetails.name!='' && this.userDetails.email!='')
    {
      //this.global.cLog(`in makeCall's method`, person);
      document.location.href = 'tel:' + person.mobile_no;
    }
    else 
    {
      this.global.showToast(`Please update your profile to make call`);
    }
  }

  makeMail(person: any) 
  {
    if(this.userDetails.name!='' && this.userDetails.email!='')
    {
      //this.global.cLog(`in makeMail's method`, person);
      document.location.href = `mailto:${person.email}?subject=Dear%20Community%20Member%20${person.name}%20`;
    }
    else 
    {
      this.global.showToast(`Please update your profile to send mail`);
    }
  }

  makeChat(person: any) 
  {
    if(this.userDetails.name!='' && this.userDetails.email!='')
    {
    	//alert(this.userDetails.chat_available);
    	console.log(this.userDetails.chat_available);
        //this.global.cLog(`in makeChat's method`, person);
        if(this.userDetails.chat_available==1)
        {
      		this.openChatPage(person);
        }
        else 
    	{
      		this.global.showToast(`Chat Option is disabled in your profile.`);
    	}
    }
    else 
    {
      this.global.showToast(`Please update your profile to make chat`);
    }
  }

  openChatPage(person: any) 
  {
  	if(this.userDetails.name!='' && this.userDetails.email!='')
    {
    	person.other_user_id = person.id;
    	this.navCtrl.push('ChatPage', { data: person, fromChatList:true,page:'community'});
    }
    else 
    {
      this.global.showToast(`Please update your profile to make chat`);
    }
  }

  removePadding() {
    this.global.cLog(`in removePadding`);

    let contentNative: HTMLElement = this.content.getNativeElement();
    let foo: any = contentNative.getElementsByClassName('scroll-content');

    this.global.cLog(`'in keyboard hide res`, contentNative, foo);
    foo[0].style.paddingBottom = '0px';
  }

  getSearchResultByDropdown(isFirst: boolean) {
    //isFirst ? null : this.showLoaders();  
    this.global.postRequest(this.global.base_path + 'Login/SerachByUsers', { searchby: this.searchType })
      .subscribe(
        res => {
          //isFirst ? null : this.hideLoaders(); 
          this.global.cLog(`getSearchResult data`, res);
          if (res.success == 'true') {
            this.userList = res.data;
            if (this.userList && this.userList.length > 0) {
              this.noData = false;
              this.userList.forEach((val, i) => {
                this.userList[i]['show'] = false;
              });
            } else {
              this.noData = true;
            }
          } else {
            this.noData = true;
            this.global.cLog(`getSearchResult error`, res);
            //this.global.showToast(res.error);
          }
        }, err => {
          this.noData = true;
          //isFirst ? null : this.hideLoaders(); 
          this.global.cLog(`getSearchResult error`, err);
        });
  }

  valueChange(ev) {
    this.global.cLog(`in valueChange`, ev);
    this.search.setValue('');
    this.getSearchResultByDropdown(false);
  }

  openPeopleDetails(_i: number) {
    this.userList.forEach((user, i) => {
      if (i == _i) {
        this.userList[i].show = !this.userList[i].show;
      } else {
        this.userList[i].show = false;
      }
    });
  }

  getHome()
  {
        this.getSearchResultByDropdown(true);

    this.search.valueChanges.debounceTime(300).subscribe(
        res => {
          this.global.cLog(`in value change and the value is `, res);
          if (res && res.length) {
            //this.showLoaders();
            this.global.postRequest(this.global.base_path + 'Login/SerachTextBox', { searchby: this.searchType, searchtext: res }).subscribe(
                res => {
                  //this.hideLoaders();
                  this.global.cLog(`getSearchResult data`, res);
                  if (res.success == 'true')
                   {
                    this.userList =[];
                    this.data= res.data;
                    this.data.forEach((val, i) => 
                    {
                      if(val.value!=undefined && val.value!='undefined')
                      {
                       this.userList.push(val);
                      }
                    });    
                    if (this.userList && this.userList.length > 0) {
                      this.noData = false;
                    } else {
                      this.noData = true;
                    }
                  } else {
                    this.noData = true;
                    this.global.cLog(`getSearchResult error`, res);
                    //this.global.showToast(res.error);
                  }
                }, err => {
                  this.noData = true;
                  this.global.cLog(`getSearchResult error`, err);
                });
          } else {
            this.getSearchResultByDropdown(true);
          }
        });
          
  }

  showLoaders()
  {
     this.loader = this.loadingCtrl.create({
                    content: 'Please wait..'
                 });
      this.loader.present();
  }
  hideLoaders()
  {
    this.loader.dismiss();
  }

  ionSelected() {
    console.log("Home Page has been selected");
    // do your stuff here
  }

  filterEmoji(event: any) 
  { 
    let newValue = event.target.value;
    newValue = newValue.replace(this.global.emojiregix, '');
    newValue = newValue.replace(this.global.alphanumeric, '');
    //event.target.value = newValue
    this.search.setValue(newValue);
    
  }


}
