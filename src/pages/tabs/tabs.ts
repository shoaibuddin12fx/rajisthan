import { GlobalProvider } from './../../providers/global/global';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Events,LoadingController, Keyboard, Platform } from 'ionic-angular';
import { SuperTabs } from 'ionic2-super-tabs';

@IonicPage()
@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {
  userDetails: any;

  @ViewChild(SuperTabs) superTabs: SuperTabs;
  // page1: string = 'HomePage';
  page1: string = 'CommunityAppNamePage';
  page2: string = 'EventPage';
  page3: string = 'ChatListPage';
  page4: string = 'ProfilePage';

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public event: Events,
    public global: GlobalProvider,
    public keyboard: Keyboard,
    public platform: Platform,
    public events: Events,public loadingCtrl:LoadingController,
  ) {
    this.userDetails = JSON.parse(localStorage.getItem('user'));
    this.listenForTabsChange();


    
    




  }


  onSelect(ev: any) 
  {
        console.log('Tab selected', 'Index: ' + ev.index, 'Unique ID: ' + ev.id);
        if(ev.index==0)
        {
           setTimeout(()=>{
                    this.events.publish('checkHome');
            },1000);
          //this.navCtrl.remove(1,1);
        }
    //
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad TabsPage');
    this.events.subscribe('setEventPage', () => {  
      // this.superTabs.selectedTabIndex = +JSON.parse(localStorage.getItem('user')).totalevents > 0 ? 1 : 0;
      if(this.userDetails.status_id==2)
      {
        this.superTabs.slideTo(1);
      }
      else
      {
        this.superTabs.slideTo(3);
      }
      this.global.cLog(`in subscribe event setEventPage`, this.superTabs.selectedTabIndex);
      this.global.hideLoader();
    });

    this.checkLiveEvent();
  }

  alertMe()
  {
    alert('yes');
  }

  listenForTabsChange() 
  {
    this.event.subscribe('select-page', (page) => {  
      this.global.cLog("in listenForTabsChange()'s select-page", page);
        console.log("user"+JSON.stringify(this.userDetails));
        if(this.userDetails.status_id==2)
        {
          this.superTabs.slideTo(2);
        }
        else
        {
          this.superTabs.slideTo(3);
        }
        //this.superTabs.slideTo(2);
    });

    this.event.subscribe('select-edit-profile', (page) => { 
      this.global.cLog("in listenForTabsChange()'s select-edit-profile", page);
      if (page == 'profile') {
        this.superTabs.slideTo(3);
      }
    });
  }

  checkLiveEvent() {
    let loader = this.loadingCtrl.create({
                    content: 'Please wait...'
                 });
                 loader.present();
    this.global.postRequest(`${this.global.base_path}Login/GetActiveEventsCount`, {})
      .subscribe(
        res => {
          loader.dismiss();
          this.global.cLog(`OtpVerify response`, res);
          if (res.success == 'true' && +res.totalevents > 0) 
          {
            if(this.userDetails.status_id==2)
            {
              this.superTabs.slideTo(1);
            }
            else
            {
              this.superTabs.slideTo(3);
            }
            //this.superTabs.slideTo(1);
            setTimeout(() => {
              this.global.hideLoader();
            }, 3000);
          } else {
            this.global.showToast(`${res.error}`);
          }
        }, err => {
          loader.dismiss();
          this.global.cLog(`OtpVerify error`, err);
        });
  }
}
