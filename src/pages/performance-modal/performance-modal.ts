import { GlobalProvider } from './../../providers/global/global';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Content, ViewController } from 'ionic-angular';
import { ThemeProvider } from '../../providers/theme/theme';

@IonicPage()
@Component({
  selector: 'page-performance-modal',
  templateUrl: 'performance-modal.html',
})
export class PerformanceModalPage {

  @ViewChild(Content) content: Content;
  performanceList: any[];
  selectedData: any = {
    performanceName: '',
    noOfParticipants: '',
    specialNeed: '',
  };
  person: any = {
    performanceName: '',
    noOfParticipants: '',
    specialNeed: '',
  };
  max_attendees: number;
  max_participants:any;
  fullperformance:any;
  showtick:boolean = true;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController,
    public global: GlobalProvider,  public theme: ThemeProvider) 
  {
    this.max_attendees = this.navParams.get('max_attendees');

    if (this.navParams.get('data')) {
      this.person = this.navParams.get('data'); console.log(this.person);
    } else {
      this.person = {
        performanceName: '',
        noOfParticipants: '',
        specialNeed: '',
      }
    }
    this.getPerformanceList();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PerformanceModalPage', this.person);
  }

  change(ip, col) {
    // this.global.log('this.ip is ', ip, col);
    if (ip) {

      let element = ip._elementRef.nativeElement;

      let textarea: HTMLElement = element.getElementsByTagName('textarea')[0];

      // set default style for textarea
      textarea.style.minHeight = '0';
      textarea.style.height = '0';

      // limit size to 96 pixels (6 lines of text)
      let scroll_height = textarea.scrollHeight;
      if (scroll_height > 80) {
        scroll_height = 80;
      }

      // apply new style
      if (scroll_height > 49) {
        col.style.height = scroll_height + 20 + 'px';
      } else {
        col.style.height = '50px';
      }
      element.style.height = scroll_height + 20 + "px";
      textarea.style.minHeight = scroll_height + "px";
      textarea.style.height = scroll_height + "px";
    }
  }

  close() {
    this.viewCtrl.dismiss();
  }

  submit() {
    this.savePerformance();
  }

  savePerformance() 
  {
    this.global.showLoader();
    this.global.postRequest(this.global.base_path + 'Login/SavePerformance', 
    {
      login_user_id: JSON.parse(localStorage.getItem('user')).id,
      event_id: this.navParams.get('id'),
      performance_id: this.person.performanceName,
      special_needs: this.person.specialNeed,
      no_of_participants: this.person.noOfParticipants
    }).subscribe(
      res => {
        this.global.hideLoader();
        this.global.cLog(`savePerformance's data`, res);
        if (res.success == 'true') {
          //this.person.performanceName = +this.person.performanceName;
          this.selectedData.performanceName = this.person.performanceName;
          this.selectedData.noOfParticipants = this.person.noOfParticipants;
          this.selectedData.specialNeed =  this.person.specialNeed;
          this.viewCtrl.dismiss(this.selectedData);
        } else {
          this.global.showToast(`${res.error}`);
        }
      }, err => {
        this.global.hideLoader();
        this.global.cLog(`savePerformance's data`, err);
        this.global.showToast(`some error in submitting data`);
      })
  }

  getPerformanceDetail() 
  {
    // this.global.showLoader();
    this.global.postRequest(`${this.global.base_path}Login/GetPerticularPerformanceofUser`, { event_id: this.navParams.get('id'), user_id: JSON.parse(localStorage.getItem('user')).id })
      .subscribe(
        res => {
          //this.global.hideLoader();
          this.global.cLog(`response of getPerformanceDetail`, res);
          if (res.success == 'true') 
          {
            this.person.performanceName = +res.performancedetail.event_performance_id;
            this.person.noOfParticipants = +res.performancedetail.no_of_participants;
            this.person.specialNeed = res.performancedetail.special_needs;
            this.fullperformance= res.performancedetail.performance_confirmed;
            if(this.fullperformance==1)
            {
               this.showtick = false;
            }
          } 
          else 
          {
            this.global.showToast(`${res.error}`);
          }
        }, err => {
          //this.global.hideLoader();
          this.global.cLog(`error of getPerformanceList`, err);
        });
  }

  getPerformanceList() 
  {
    this.global.showLoader();
    this.global.postRequest(`${this.global.base_path}Login/PerformanceList`, { event_id: this.navParams.get('id') })
      .subscribe(
        res => {
          this.global.hideLoader();
          this.global.cLog(`response of getPerformanceList`, res);
          if (res.success == 'true' && res.performance.length > 0) {
            this.performanceList = res.performance;
            this.max_participants = this.performanceList[0].max_participants;
            // this.person.performanceName = this.performanceList[0].id;
              this.getPerformanceDetail();
          } else {
            this.global.showToast(`${res.error}`);
          }
        }, err => {
          this.global.hideLoader();
          this.global.cLog(`error of getPerformanceList`, err);
        });
  }

  removePadding() {
    this.global.cLog(`in removePadding`);

    let contentNative: HTMLElement = this.content.getNativeElement();
    let foo: any = contentNative.getElementsByClassName('scroll-content');

    this.global.cLog(`'in keyboard hide res`, contentNative, foo);
    foo[0].style.paddingBottom = '0px';
  }
  changeparticipants()
  {

    this.performanceList.forEach((val,key)=>{
      if(val.id==this.person.performanceName)
      {
        this.max_participants =this.performanceList[key].max_participants;
        //this.person.noOfParticipants='';
      }
    });
    //alert(this.person.performanceName);
  }

  filterEmoji(control: string, event: any) {

    let newValue = event.target.value;
    newValue = newValue.replace(this.global.emojiregix, "");
    newValue = newValue.replace(this.global.alphanumeric, "");
    event.target.value = newValue;
    
  }

}
