import { GlobalProvider } from './../../providers/global/global';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Events,LoadingController } from 'ionic-angular';
import {DomSanitizer} from '@angular/platform-browser';

@IonicPage()
@Component({
  selector: 'page-chat-list',
  templateUrl: 'chat-list.html',
})
export class ChatListPage {

  noData: boolean;
  chatList: any[];
  personList: any[];
  loader:any;
  userDetails:any;
  index:any;
  chatListData:any={};

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public global: GlobalProvider,
    public sanitizer: DomSanitizer,
    public events: Events,public loadingCtrl:LoadingController
  ) {
    // this.fillList();
    // if(this.navParams.get('index')!='' && this.navParams.get('index')!=undefined && this.navParams.get('index')!=null)
    // {
    //   if(this.navParams.get('msg')!='' && this.navParams.get('msg')!=undefined && this.navParams.get('msg')!=null)
    //   {
    //     this.chatList[this.navParams.get('index')].message = this.navParams.get('msg');
    //   }
    // }
    this.userDetails = JSON.parse(localStorage.getItem('user'));
    if(this.userDetails.chat_available==1)
    {
        this.events.subscribe('select-page2', data => { //alert("inchat");
          setTimeout(() => {
            this.getChatListData(false);
            //alert(this.navCtrl.getActive().name);
            if(this.navCtrl.getActive().name!='' && this.navCtrl.getActive().name!=undefined && this.navCtrl.getActive().name!='' && this.navCtrl.getActive().name!='ChatPage')
            {
              this.navCtrl.push('ChatPage', { data: data }); //, fromChatList: true
            }
            else
            {
              this.chatListData.entry_date_time= new Date().toISOString();
              this.chatListData.from_user_id= data.data.from_sender_id;
              this.chatListData.message= data.data.body;
              this.chatListData.to_user_id= data.data.to_sender_id;
              this.chatListData.id= data.data.from_sender_id;
              this.chatListData.user_image= data.data.user_image ? data.data.user_image : null;
              this.chatListData.message_id= data.data.message_id;
              this.chatListData.name = data.data.title;
              var test = this.chatListData.entry_date_time.replace(' ', 'T');
                    this.chatListData.entry_date_time1= test;

              this.events.publish('chatPageData', {data:this.chatListData} );
            }
          }, 250);
        });
        this.getChatListData(true);
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChatListPage');
  }

  fillList() {
    this.personList = [
      {
        image: 'assets/icon/sidebar-profile-photo.png',
        name: 'Linda Natasha',
        message: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        time: new Date(),
      },
      {
        image: 'assets/icon/sidebar-profile-photo.png',
        name: 'Steven Swap',
        message: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        time: new Date(),
      },
      {
        image: 'assets/icon/sidebar-profile-photo.png',
        name: 'Carza Heldon',
        message: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        time: new Date(),
      },
      {
        image: 'assets/icon/sidebar-profile-photo.png',
        name: 'Richard Steve',
        message: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        time: new Date(),
      },
      {
        image: 'assets/icon/sidebar-profile-photo.png',
        name: 'Natasha Chaw',
        message: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        time: new Date(),
      },
      {
        image: 'assets/icon/sidebar-profile-photo.png',
        name: 'Natasha Chaw',
        message: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        time: new Date(),
      },
      {
        image: 'assets/icon/sidebar-profile-photo.png',
        name: 'Linda Natasha',
        message: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        time: new Date(),
      },
    ];
  }

  showLoaders()
  {
     this.loader = this.loadingCtrl.create({
                    content: 'Please wait...'
                 });
     this.loader.present();
  }
  hideLoaders()
  {
    this.loader.dismiss();
  }

  openChatDetails(person, i) 
  {
    this.navCtrl.push('ChatPage', { data: this.chatList[i], fromChatList: true,index:i });
  }

  edit() {
    this.global.cLog(`in edit()`);
  }

  getChatListData(showLoader: boolean) 
  {
    showLoader ? this.showLoaders() : null;
    this.global.postRequest(`${this.global.base_path}Login/Chatlist`, { login_user_id: JSON.parse(localStorage.getItem('user')).id }).subscribe(
        res => {
          showLoader ? this.hideLoaders() : null;
          this.global.cLog(`getChatListData's response is`, res);
          if (res.success == 'true') 
          {
            this.chatList = res.chatlist;
            if (this.chatList.length > 0) {
              this.noData = false;
              this.chatList.forEach(element => {

                  var test = element.time.replace(' ', 'T');
                  element.time1 = test;

                if (element.image) {
                  element.image = `${this.global.image_base_path}user/${element.image}`;
                } else {
                  element.image = this.global.defaultImage;
                }
                element["user_image"] = element.image;
              });
            } else {
              this.noData = true;
            }
          } else {
            this.noData = true;
          }
        }, err => {
          showLoader ? this.hideLoaders() : null;
          this.global.cLog(`getChatListData's error is`, err);
        }
      );
  }

  ionViewDidEnter()
 {
   this.userDetails = JSON.parse(localStorage.getItem('user'));
   this.getChatListData(false);
 }


}
