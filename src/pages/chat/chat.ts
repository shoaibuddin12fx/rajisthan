import { GlobalProvider } from './../../providers/global/global';
import { Component, ViewChild, NgZone } from '@angular/core';
import { Keyboard } from '@ionic-native/keyboard';
import { IonicPage, NavController, NavParams, Content, Events, LoadingController,ToastController } from 'ionic-angular';
import {DomSanitizer} from '@angular/platform-browser';
import { ThemeProvider } from '../../providers/theme/theme';
import { Platform } from 'ionic-angular/platform/platform';

@IonicPage()
@Component({
  selector: 'page-chat',
  templateUrl: 'chat.html',
})
export class ChatPage {

  noData: boolean;
  page: number = 1;
  chatData: any[] = [];
  dummyChat: any[];
  inputText: string;
  chatListData: any;
  userDetails: any;
  user_image: string;
  fromChatList: boolean = false;
  loader:any;
  index:any;
  lastMsg:any;
  fromPage:any;
  mode_of_communication:any;
  dateformat: any;
  defaultTheme: any;
  plusGMTTIme: any;

  @ViewChild('content') content: Content;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public events: Events,
    public keyboard: Keyboard,
    public global: GlobalProvider,
    public theme: ThemeProvider,
    public platform: Platform,
    public sanitizer: DomSanitizer,
    public zone: NgZone,public toastCtrl: ToastController,public loadingCtrl:LoadingController
    ) {
    // this.fillDummyChat();
    this.chatListData = this.navParams.get('data'); //alert(JSON.stringify(this.chatListData));
    this.index = this.navParams.get('index');
    this.fromChatList = this.navParams.get('fromChatList') || false;
    this.fromPage = this.navParams.get('page') || '';

    this.defaultTheme = this.theme['defaultTheme'];
    this.dateformat = this.defaultTheme['date_format'] + ' HH:mm:ss' ;
    this.plusGMTTIme = this.convertTImeZoneFormat(this.defaultTheme['time_zone']);

    if(this.fromChatList) 
    {
      this.chatListData.id = this.chatListData.other_user_id;
      //alert(this.chatListData.id);
    } 
    else 
    {
       this.chatListData.entry_date_time= new Date().toISOString();
       this.chatListData.from_user_id= this.chatListData.data.from_sender_id;
       this.chatListData.message= this.chatListData.data.body;
       this.chatListData.id= this.chatListData.data.from_sender_id;
       this.chatListData.to_user_id= this.chatListData.data.to_sender_id;
       if(this.chatListData.user_image!='assets/icon/sidebar-profile-photo.png')
       {
          this.chatListData.user_image= this.chatListData.user_image ? this.global.image_base_path+'user'+this.chatListData.user_image : null;
       }
       
       this.chatListData.message_id= this.chatListData.data.message_id;
       this.chatListData.name = this.chatListData.data.title;
    
      if(this.chatListData.user_image!='assets/icon/sidebar-profile-photo.png') 
      {
        this.chatListData.user_image = this.global.image_base_path+'user'+this.chatListData.user_image;
      }
      else 
      {
        this.chatListData.user_image = this.global.defaultImage;
      }
      this.scrollToBottom();
    }
    this.getChatData();
    this.listenForPushMessages();

    this.keyboard.onKeyboardHide().subscribe( () => {
      if (window.pageYOffset != 0) {
        window.scrollTo(0, 0);
      }

    })

    this.keyboard.onKeyboardShow().subscribe( () => {
      this.scrollToBottom();
    })

  }

  /*listenForPushMessages() {
    this.events.subscribe('chatPageData', data => {
      this.global.cLog(`got the push data`, data, this.chatData);

      //do the logic

      this.zone.run(()=>{
        this.chatData.push(data.data);
        this.global.cLog(`chat data after push`, this.chatData);
        this.scrollToBottom();
      })
    });
  } */

  listenForPushMessages() 
  {
   this.events.subscribe('chatPageData', data => {
     this.global.cLog(`got the push data`, data, this.chatData);
     data.message = data.body;
     data.to_user_id = data.to_sender_id;
     data.from_user_id = data.from_sender_id;
     data.entry_date_time = new Date().toISOString();
     data.entry_date_time1 = new Date().toISOString(); //data.entry_time;
     //do the logic

     this.zone.run(()=>{
       this.chatData.push(data);
       this.global.cLog(`chat data after push`, this.chatData);
       this.scrollToBottom();
     })
   });
  }


  ionViewDidEnter() {
    
    
    localStorage.setItem('chatPage', "true");
  }

  ionViewDidLeave() 
  {
    // this.keyboard.disableScroll(true);
    if(this.fromPage!='community')
    {
      this.events.unsubscribe('chatPageData');
      localStorage.removeItem('chatPage');
      this.navCtrl.setRoot('ChatListPage', { msg:this.lastMsg,index:this.index });
    }
  }

  ionViewDidLoad() 
  {
    this.userDetails = JSON.parse(localStorage.getItem('user'));
    if (this.userDetails.user_image) 
    {
      this.user_image = this.global.image_base_path + 'user/' + this.userDetails.user_image;
    } 
    else {
      this.user_image = this.global.defaultImage;
    }
    console.log('ionViewDidLoad ChatPage', this.chatListData, this.fromChatList);
    this.scrollToBottom(250);
  }

  fillDummyChat() 
  {
    this.dummyChat = [
      {
        isMe: false,
        userImage: 'assets/icon/sidebar-profile-photo.png',
        message: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
        time: new Date()
      },
      {
        isMe: true,
        userImage: 'assets/icon/sidebar-profile-photo.png',
        message: 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
        time: new Date()
      },
      {
        isMe: false,
        userImage: 'assets/icon/sidebar-profile-photo.png',
        message: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
        time: new Date()
      },
      {
        isMe: true,
        userImage: 'assets/icon/sidebar-profile-photo.png',
        message: 'Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
        time: new Date()
      },
      {
        isMe: false,
        userImage: 'assets/icon/sidebar-profile-photo.png',
        message: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
        time: new Date()
      }
    ];
  }

  send() 
  {


    // this.keyboard.close();
    this.global.cLog('Sending message');
    /*if(this.mode_of_communication.includes('2'))
    { */

      if (this.inputText.length > 0) {
      this.inputText = this.inputText.trim();
      
      let pushData = {
        entry_date_time: new Date().toISOString(),
        entry_date_time1: new Date().toISOString(),
        from_user_id: JSON.parse(localStorage.getItem('user')).id,
        message: this.inputText,
        to_user_id: this.chatListData.id,
      };

      let postData = {
        to_user_id: this.chatListData.id,
        login_user_id: JSON.parse(localStorage.getItem('user')).id,
        message: this.inputText,
      }

      this.sendMessage(postData, pushData);
      }
    //}
  }

  scrollToBottom(timeout: number = 0) {
    setTimeout(() => {
      this.content.scrollToBottom();
    }, 100);
  }

  getChatData() 
  {
    //alert(JSON.parse(localStorage.getItem('user')).id+','+this.chatListData.id);
    this.showLoaders();
    let data = {
      to_user_id: this.chatListData.id,
      login_user_id: JSON.parse(localStorage.getItem('user')).id,
      page: this.page
    }
    this.global.postRequest(`${this.global.base_path}Login/Conversation`, data)
      .subscribe(
        res => {
          this.hideLoaders();
          this.global.cLog(`getChatData's response is`, res);

          if (res.success == 'true') 
          {
              this.chatData = res.conversation;
              this.chatData.forEach((res,i)=>{
                this.lastMsg= this.chatData[i].message;
                var test = this.chatData[i].entry_date_time.replace(' ', 'T');
                
                if(this.chatData[i].user_image == '' || this.chatData[i].user_image ==undefined)
                {
                   this.chatData[i].user_image = this.global.defaultImage;
                }
                console.log("img"+this.chatData[i].user_image);
                this.chatData[i].entry_date_time1 = test;
                console.log('tttt'+this.chatData[i].entry_date_time);
              });
            this.noData = false;
            this.scrollToBottom();
          } else {
            this.noData = true;
          }
        }, err => {
          this.noData = true;
          this.hideLoaders();
          this.global.cLog(`getChatData's error is`, err);
        }
      );
  }

  async sendMessage(postData, pushData) 
  {
      /*var test = pushData.entry_date_time.replace(' ', 'T');
                pushData.entry_date_time1 = test;*/
    pushData['status'] = 'pending';
    this.chatData.push(pushData);
    this.scrollToBottom();
    // this.content.scrollToBottom();
    
    this.global.postRequest(`${this.global.base_path}Login/SendMessage`, postData)
      .subscribe(
        res => {
          this.global.cLog(`getChatData's response is`, res);
          this.lastMsg=this.inputText;
          if (res.success == 'true') 
          {
            this.noData = false;
            pushData['status'] = 'completed';
            this.inputText = '';
            //pushData['id'] = res.id;
          } 
          else 
          {
            this.noData = false;
            pushData['status'] = 'error';
            this.global.showToast(`${res.error}`);
          }
        }, err => {
          // this.noData = true;
          this.global.cLog(`getChatData's error is`, err);
        }
      );
  }

  showLoaders()
  {
     this.loader = this.loadingCtrl.create({
                    content: 'Please wait..'
                 });
      this.loader.present();
  }
  hideLoaders()
  {
    this.loader.dismiss();
  }

  getDateFromString(datestr){
    return datestr.split('T')[0];
  }

  getTimeFromString(datestr){
    return datestr.split('T')[1];
  }

  getTimezoneTime(chat){

    console.log(chat);
    return chat.entry_date_time1;

  }

  convertTImeZoneFormat(rlt){
    
    function n(n){
        return n > 9 ? "" + n: "0" + n;
    }
    
    // +0:+0 -> +0000 // +2:15 -> +0215
    var plusMinus = (rlt.charAt(0) == '-') ? '-' : '+';
    rlt = rlt.replace(/\+/g, '');
    var hours = n(rlt.split(':')[0]);
    var minutes = n(rlt.split(':')[1]);
    return plusMinus + hours + minutes;
  }

}