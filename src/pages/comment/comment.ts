import { GlobalProvider } from './../../providers/global/global';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController,Content, ViewController } from 'ionic-angular';
import { ThemeProvider } from '../../providers/theme/theme';

/**
 * Generated class for the CommentPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-comment',
  templateUrl: 'comment.html',
})
export class CommentPage {
	 @ViewChild(Content) content: Content;
	feedback:any;
	user_id:any;
    event_id:any;
    loader:any;


  constructor(public navCtrl: NavController, public navParams: NavParams,public theme:ThemeProvider,public global:GlobalProvider, public viewCtrl: ViewController,public loadingCtrl: LoadingController) 
  {
    this.user_id = this.navParams.get('user_id');
    this.event_id = this.navParams.get('event_id');
    if(this.event_id!='' && this.event_id!=undefined && this.event_id!=null)
    {
      this.global.postRequest(`${this.global.base_path}Login/GetEventFeedback`, {user_id:localStorage.getItem('user_id'),event_id:this.event_id}).subscribe(res => {
         this.feedback = res.feedback;
      });
    }
  }

  change(ip, col) 
  {
    // this.global.log('this.ip is ', ip, col);
    if (ip) {

      let element = ip._elementRef.nativeElement;

      let textarea: HTMLElement = element.getElementsByTagName('textarea')[0];

      // set default style for textarea
      textarea.style.minHeight = '0';
      textarea.style.height = '0';

      // limit size to 96 pixels (6 lines of text)
      let scroll_height = textarea.scrollHeight;
      if (scroll_height > 80) {
        scroll_height = 80;
      }

      // apply new style
      if (scroll_height > 49) {
        col.style.height = scroll_height + 20 + 'px';
      } else {
        col.style.height = '50px';
      }
      element.style.height = scroll_height + 20 + "px";
      textarea.style.minHeight = scroll_height + "px";
      textarea.style.height = scroll_height + "px";
    }
  }


  saveevent()
  {
    if(this.feedback!='' && this.feedback!=undefined && this.feedback!=null)
    {
    	let data= {"user_id":localStorage.getItem('user_id'),"event_id":this.event_id,"feedback_text":this.feedback};
      	this.showLoaders();
      	this.global.postRequest(`${this.global.base_path}Login/UpdateEventFeedback`, data).subscribe(res => {
           this.hideLoaders();
          if (res.success == 'true') 
          {
             this.global.showToast(`${res.message}`);
             this.viewCtrl.dismiss();
          }
      	}, err => {
           this.hideLoaders();
     	});
    }
  }

   removePadding() {
    this.global.cLog(`in removePadding`);

    let contentNative: HTMLElement = this.content.getNativeElement();
    let foo: any = contentNative.getElementsByClassName('scroll-content');

    this.global.cLog(`'in keyboard hide res`, contentNative, foo);
    foo[0].style.paddingBottom = '0px';
  }

  showLoaders()
  {
     this.loader = this.loadingCtrl.create({
                    content: 'Please wait...'
                 });
     this.loader.present();
  }
  hideLoaders()
  {
    this.loader.dismiss();
  }

  close() {
    this.viewCtrl.dismiss();
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad CommentPage');
  }

  filterEmoji(event: any) 
  {
    let newValue = event.target.value;
    newValue = newValue.replace(this.global.emojiregix, '');
    event.target.value = newValue;
  }

}
