import { GlobalProvider } from './../../providers/global/global';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController } from 'ionic-angular';
import { ThemeProvider } from '../../providers/theme/theme';

@IonicPage()
@Component({
  selector: 'page-promotion-discount',
  templateUrl: 'promotion-discount.html',
})
export class PromotionDiscountPage {

  promotionAndDiscountData: any;
  noData: boolean;
  loader:any;
  promotions:any=[];
  
  constructor( public navCtrl: NavController, public navParams: NavParams, public global: GlobalProvider, public theme: ThemeProvider,public loadingCtrl: LoadingController
  ) {
    this.getPromotionAndDiscountData();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PromotionDiscountPage');
  }

  // openDetails(i: number) {
  //   this.global.cLog('openDetails');
  //   this.navCtrl.push('PromotionDetailsPage', { data: this.promotionAndDiscountData.New[i] });
  // }

  // openCat(_i: number) {
  //   this.promotionAndDiscountData.New.forEach((people, i) => {
  //     if (i == _i) {
  //       this.promotionAndDiscountData.New[i].show = !this.promotionAndDiscountData.New[i].show;
  //     } else {
  //       this.promotionAndDiscountData.New[i].show = false;
  //     }
  //   });
  // }
  openCat(_i: number) {
    this.promotions.forEach((people, i) => {
      if (i == _i) {
        this.promotions[i].show = !this.promotionAndDiscountData.show;
      } else {
        this.promotions[i].show = false;
      }
    });
  }

  openDetails(data) {
    this.global.cLog('openDetails');
    this.navCtrl.push('PromotionDetailsPage', { data: data });
  }

  getPromotionAndDiscountData() {
    this.showLoaders();
    this.global.postRequest(this.global.base_path + 'Login/PromtionDiscount', {})
      .subscribe(
        res => {
          this.global.cLog(`getPromotionAndDiscountData's res`, res);
          this.hideLoaders();
          if (res.success == 'true') {
            this.noData = false;
            this.promotionAndDiscountData = res.data;
            this.promotionAndDiscountData.forEach(e => {
              e["show"] = false;
              this.promotions.push(e);
            });
            console.log(this.promotions);
            // if (this.promotionAndDiscountData.New.length == 1) {
            //   this.promotionAndDiscountData.New[0].show = true;
            // }
          } else {
            this.global.showToast(`${res.error}`);
            this.noData = true;
          }
        }, err => {
          this.noData = true;
          this.hideLoaders();

        });
  }

  showLoaders()
  {
     this.loader = this.loadingCtrl.create({
                    content: 'Please wait...'
                 });
      this.loader.present();
  }
  hideLoaders()
  {
    this.loader.dismiss();
  }



}
