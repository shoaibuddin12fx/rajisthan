import { GlobalProvider } from './../../providers/global/global';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Content,LoadingController } from 'ionic-angular';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Keyboard } from '@ionic-native/keyboard';

@IonicPage()
@Component({
  selector: 'page-change-pin',
  templateUrl: 'change-pin.html',
})
export class ChangePinPage {

  changePinForm: FormGroup;
  isFormInvalid: boolean = false;
  otpResponseValue: any;
  fromLogin: boolean;
  loader:any;
  @ViewChild(Content) content: Content;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public fb: FormBuilder,
    public global: GlobalProvider,
    public keyboard: Keyboard,public loadingCtrl:LoadingController
  ) {
    this.fromLogin = this.navParams.get('fromLogin');

    if (this.fromLogin) {
      this.otpResponseValue = JSON.parse(localStorage.getItem('otp-res-value'));
    } else {
      this.otpResponseValue = JSON.parse(localStorage.getItem('user'));
    }

    this.global.cLog(`this from login value is`, this.fromLogin, this.otpResponseValue);
    this.initForm();
  }

  initForm() {
    if (this.fromLogin) {
      this.changePinForm = this.fb.group({
        oldPin: [null, [Validators.required, Validators.minLength(6), Validators.maxLength(6)]],
        newPin: [null, [Validators.required, Validators.minLength(6), Validators.maxLength(6)]]
      });
    } else {
      this.changePinForm = this.fb.group({
        oldPin: [null, [Validators.required, Validators.minLength(6), Validators.maxLength(6)]],
        newPin: [null, [Validators.required, Validators.minLength(6), Validators.maxLength(6)]],
        conformPin: [null, [Validators.required, Validators.minLength(6), Validators.maxLength(6)]]
      });

    }
  }

  showLoaders()
  {
     this.loader = this.loadingCtrl.create({
                    content: 'Please wait..'
                 });
      this.loader.present();
  }
  hideLoaders()
  {
    this.loader.dismiss();
  }

  ionViewDidLoad() {

    console.log('ionViewDidLoad ChangePinPage', this.fromLogin);

    this.changePinForm.controls['oldPin'].valueChanges.subscribe(res => {
      if (res && res.length > 6) {
        this.changePinForm.controls['oldPin'].setValue(this.changePinForm.controls['oldPin'].value.slice(0, 6));
      }
    });

    this.changePinForm.controls['newPin'].valueChanges.subscribe(res => {
      if (res && res.length > 6) {
        this.changePinForm.controls['newPin'].setValue(this.changePinForm.controls['newPin'].value.slice(0, 6));
      }
    });

    if (!this.fromLogin) {

      this.changePinForm.controls['conformPin'].valueChanges.subscribe(res => {
        if (res && res.length > 6) {
          this.changePinForm.controls['conformPin'].setValue(this.changePinForm.controls['conformPin'].value.slice(0, 6));
        }
      });

    }

    // this.keyboard.onKeyboardHide().subscribe(
    //   res => {
    //     this.global.cLog(`in onKeyboardHide`, res);
    //     this.removePadding();
    //   }, err => {
    //     this.removePadding();
    //   });

    // this.keyboard.onKeyboardShow().subscribe(
    //   res => {
    //     this.global.cLog(`in onKeyboardHide`, res);
    //     this.removePadding();
    //   }, err => {
    //     this.removePadding();
    //   });
  }

  changePassword() {
    this.global.cLog(`in changePassword and the form is`, this.changePinForm);
    if (this.changePinForm.valid) {
      if (this.fromLogin) {
        //alert(this.otpResponseValue.id);
        if (this.changePinForm.controls['oldPin'].value == this.changePinForm.controls['newPin'].value) {
          this.setPasscode(
            {
              user_id: (localStorage.getItem('user_id'))?localStorage.getItem('user_id'):this.otpResponseValue.id,
              pin: this.changePinForm.controls['newPin'].value
            }
          );
        } else {
          this.global.showToast(`PIN does not match`);
        }
      } else {
        if (this.changePinForm.controls['conformPin'].value == this.changePinForm.controls['newPin'].value) {
          this.global.cLog('form is valid');
          // this.navCtrl.setRoot('LoginPage', { signInData: true });
          var x = (localStorage.getItem('user_id'))?localStorage.getItem('user_id'):this.otpResponseValue.id;
          this.setPasscode({ user_id: x, pin: this.changePinForm.controls['newPin'].value })
        } else {
          this.global.showToast(`PIN does not match`);
        }
      }
    } else {
      this.isFormInvalid = true;
    }
  }

  setPasscode(data: any) {
    this.showLoaders();
    this.global.postRequest(`${this.global.base_path}Login/SetPasscode`, data)
      .subscribe(
        res => {
          this.hideLoaders();
          this.global.cLog(`SetPasscode res`, res);
          //alert(this.fromLogin+'-'+JSON.stringify(res)+'-uid'+localStorage.getItem('user_id'));
          if (res.success == 'true') 
          {
            this.global.showToast(`${res.message}, Please Login now`);
            if(localStorage.getItem('user_id'))
            {
              if(this.fromLogin)
              {
                localStorage.removeItem('user_id');
                this.navCtrl.setRoot('LoginPage', { signInData: true });
              }
              else
              {
                this.navCtrl.setRoot('MenuPage', { data: null });
              }
            }
            else
            {
              this.navCtrl.setRoot('LoginPage', { signInData: true });
            }
          } 
          else 
          {
            //alert('else');
            this.global.showToast(`${res.error}`);
          }
        }, err => {
          this.hideLoaders();
          this.global.cLog(`SetPasscode err`, err);
        });
  }

  removePadding() {
    this.global.cLog(`in removePadding`);
    let contentNative: HTMLElement = this.content.getNativeElement();
    let foo: any = contentNative.getElementsByClassName('scroll-content');
    this.global.cLog(`'in keyboard hide res`, contentNative, foo);
    foo[0].style.paddingBottom = '0px';
  }

  filterEmoji(control: string, event: any) 
  {
    let newValue = event.target.value;
    newValue = newValue.replace(this.global.emojiregix, "");
    //event.target.value = newValue;

    newValue = newValue.replace(this.global.numericregix, "");
    
    //event.target.value = newValue;
    this.changePinForm.controls[control].setValue(newValue);
  }

}
