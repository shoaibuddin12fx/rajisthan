import { GlobalProvider } from './../../providers/global/global';
import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, Content, Events,LoadingController } from 'ionic-angular';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Keyboard } from '@ionic-native/keyboard';
import { ThemeProvider } from '../../providers/theme/theme';
import {SidebarService} from '../../services/sidebar.service';
import {DomSanitizer} from '@angular/platform-browser';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage 
{
  loader:any;
  enterButton: boolean = false;
  loginRes: any;
  OtpVerify: any;
  resending: any = { time: 50, value: false };
  signInData: any;
  loginForm: FormGroup;
  signUpForm: FormGroup;
  isFormInvalid: boolean = false;
  signInOtp: string;
  resendText: string = 'Send OTP';
  defaultTheme:any;
  @ViewChild(Content) content: Content;
  totalevents:any=0;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public fb: FormBuilder,
    public global: GlobalProvider,
    public sanitizer: DomSanitizer,
    public keyboard: Keyboard,
    public theme: ThemeProvider,
    public events: Events,public sidebarService: SidebarService,public loadingCtrl:LoadingController
  ) 
  {
    //this.defaultTheme=this.theme.defaultTheme; 

    this.initForm();
    this.signInData = this.navParams.get('signInData');
    this.global.cLog(this.signInData);
    this.events.subscribe('set-login', d => {
      this.global.cLog(`d is`, d)
      this.signInData = d;
    });
  }

  ionViewDidLoad() 
  {
    console.log('ionViewDidLoad LoginPage');
   
    this.loginForm.controls['mobile'].valueChanges.subscribe(res => {
      if (res && res.length > this.theme.defaultTheme.length_of_mobile_no) {
        this.loginForm.controls['mobile'].setValue(this.loginForm.controls['mobile'].value.slice(0, this.theme.defaultTheme.length_of_mobile_no));
      }
      if (res && res.length == this.theme.defaultTheme.length_of_mobile_no) {
        this.global.cLog('mobile res is', res);
        // this.requestOTP({ mobile: `${res}` });
      }
    });

    this.loginForm.controls['otp'].valueChanges.subscribe(res => {
      if (res && res.length > 10) {
        this.loginForm.controls['otp'].setValue(this.loginForm.controls['otp'].value.slice(0, 10));
      }
    });

    this.signUpForm.controls['mobile'].valueChanges.subscribe(res => {
      if (res && res.length > this.theme.defaultTheme.length_of_mobile_no) {
        this.signUpForm.controls['mobile'].setValue(this.signUpForm.controls['mobile'].value.slice(0, this.theme.defaultTheme.length_of_mobile_no));
      }
    });

    this.signUpForm.controls['pin'].valueChanges.subscribe(res => {
      if (res && res.length > 6) {
        this.signUpForm.controls['pin'].setValue(this.signUpForm.controls['pin'].value.slice(0, 6));
      }
    });

    //this.keyboard.disableScroll(true)

    // this.keyboard.onKeyboardHide().subscribe(
    //   res => {
    //     this.global.cLog(`in onKeyboardHide`, res);
    //     this.removePadding();
    //   }, err => {
    //     this.removePadding();
    //   });

    // this.keyboard.onKeyboardShow().subscribe(
    //   res => {
    //     this.global.cLog(`in onKeyboardHide`, res);
    //     this.removePadding();
    //   }, err => {
    //     this.removePadding();
    //   });
  }

  codeInput(event) 
  {
    //48 - 57
    let pass = /[4][8-9]{1}/.test(event.charCode) || /[5][0-7]{1}/.test(event.charCode);
    if (!pass) {
      return false;
    }
  }

  removePadding() {
    this.global.cLog(`in removePadding`);

    let contentNative: HTMLElement = this.content.getNativeElement();
    let foo: any = contentNative.getElementsByClassName('scroll-content');

    this.global.cLog(`'in keyboard hide res`, contentNative, foo);
    foo[0].style.paddingBottom = '0px';
  }

  requestOTP(data: any, isResend: boolean = false) {
    this.showLoaders();
    this.global.postRequest(`${this.global.base_path}Login/RegisterRequestOtp`, data)
      .subscribe(
        res => {
          this.hideLoaders();
          this.global.cLog(`res is `, res);
          if (res.success == 'true') {
            this.global.showToast(`${res.message}`);
            if (isResend) {
              this.resending.value = true;
              let interval = setInterval(() => {
                this.global.cLog(`in setInterval`, this.resending);
                this.resending.time--;
                if (this.resending.time < 0) {
                  this.resending.value = false;
                  this.resending.time = 50;
                  clearInterval(interval);
                }
              }, 1000);
            }
          } else {
            if (res.error == 'Mobile Number is already Registered.') {
              this.global.showToast(`${res.error} Try logging in.`);
              this.OtpVerify = res;
            } else {
              this.global.showToast(`${res.error}`);
            }
          }
        },
        err => {
          this.hideLoaders();
          this.global.cLog(`err is `, err);
        }
      );
  }

  login_api(data: any) 
  {
    // this.navCtrl.setRoot('ProfilePage', { data: { fromLogin: true } });
    data["fcm_token"] = localStorage.getItem('fcm_token');
    this.showLoaders();
    this.global.postRequest(`${this.global.base_path}Login/Login`, data)
      .subscribe(
        res => {
          this.hideLoaders();
          this.global.cLog(`login response`, res);
          if (res.success == 'true') 
          {
            this.global.showToast(`${res.message}`);
            this.loginRes = res;
            this.toggleSidebar({userid:res.id,status:res.status_id});
            localStorage.setItem('user', JSON.stringify(res));
            if (res.name && res.name.length > 0) {
              //setTimeout(() => {
                localStorage.setItem('user_id',res.id);
                this.navCtrl.setRoot('MenuPage', { data: null }); 
                if (+res.totalevents > 0) 
                {
                  
                  this.global.cLog(`firing event setEventPage`);
                  this.events.publish('setEventPage');
                }
                else
                {
                  this.events.publish('setHomePage');
                }
              //}, 500);
            } 
            else 
            {
              localStorage.setItem('user_id',res.id);
              //setTimeout(() => {

                //this.navCtrl.setRoot('ProfilePage', { data: { fromLogin: true } });
                this.navCtrl.setRoot('MenuPage', { data: null }); 
                  this.global.cLog(`firing event setEventPage`);
                  this.events.publish('setProfilePage');``
              //}, 1000);
            }
          } 
          else 
          {
            if(res.error!="Pin is not match" && res.error!="Mobile no is not Registered.")
            {
               localStorage.setItem('user_id',res.id);
               localStorage.setItem('user', JSON.stringify(res));
               this.navCtrl.setRoot('MenuPage', { data: null }); 
               this.global.cLog(`firing event setEventPage`);
               
            }
            else
            { 
              this.global.showToast(`${res.error}`);
            } 
          }
        }, err => {
          this.hideLoaders();
          this.global.cLog(`login error`, err);
        });
  }

  showLoaders()
  {
     this.loader = this.loadingCtrl.create({
                    content: 'Please wait...'
                 });
      this.loader.present();
  }
  hideLoaders()
  {
    this.loader.dismiss();
  }




  verifyOTP(data: any) {
    this.showLoaders();
    this.global.postRequest(`${this.global.base_path}Login/OtpVerify`, data)
      .subscribe(
        res => {
          this.hideLoaders();
          this.global.cLog(`OtpVerify response`, res);
          if (res.success == 'true') {
            this.global.showToast(`${res.message}, Set new pin now.`);
            this.OtpVerify = res;
            localStorage.setItem('otp-res-value', JSON.stringify(res));
            setTimeout(() => {
              // this.signInData = true;
              // this.signUpForm.controls['mobile'].setValue(data.mobile);
              this.navCtrl.setRoot('ChangePinPage', { fromLogin: true });
            }, 500);
          } else {
            this.global.showToast(`${res.error}`);
          }
        }, err => {
          this.hideLoaders();
          this.global.cLog(`OtpVerify error`, err);
        });
  }

  toggleSidebar(data) 
  {
    this.sidebarService.toggleSidebarVisibilty(data)
  }

  initForm() 
  {
    this.loginForm = this.fb.group({
      mobile: [null, [Validators.required, Validators.minLength(this.theme.defaultTheme.length_of_mobile_no), Validators.maxLength(this.theme.defaultTheme.length_of_mobile_no)]],
      otp: [null, [Validators.required, Validators.minLength(4), Validators.maxLength(10)]]
    });

    this.signUpForm = this.fb.group({
      mobile: [null, [Validators.required, Validators.minLength(this.theme.defaultTheme.length_of_mobile_no), Validators.maxLength(this.theme.defaultTheme.length_of_mobile_no)]],
      pin: [null, [Validators.required, Validators.minLength(6), Validators.maxLength(6)]]
    })
  }

  resendOTP() {
    this.global.cLog('Resend OTP');
    // this.navCtrl.setRoot('ChangePinPage', { fromLogin: true });
    if (!this.resending.value) {

      if (this.loginForm.controls['mobile'].value && this.loginForm.controls['mobile'].value.length == this.theme.defaultTheme.length_of_mobile_no) {
        this.enterButton = true;
        if (this.resending.time == 50) {
          if (this.resendText == 'Send OTP') {
            this.resendText = 'Re-send OTP';
          }
          this.requestOTP({ mobile: `${this.loginForm.controls['mobile'].value}` }, true);
        }
      } else {
        this.global.showToast(`Mobile number not correct`);
      }
    }
  }

  login() {
    this.global.cLog('Logging in', this.loginForm, this.signInData);

    if (this.signInData) {
      if (this.signUpForm.valid) {
        this.global.cLog('form is valid');
        // this.setPasscode({ user_id: this.OtpVerify.id, pin: this.signUpForm.controls['pin'].value });
        let data = this.signUpForm.value;
        data["uuid"] = localStorage.getItem('uuid');
        this.login_api(data);
      } else {
        this.isFormInvalid = true;
      }
    } else {
      if (this.loginForm.valid) {
        this.global.cLog('form is valid');
        let data = this.loginForm.value;
        data.mobile = `${data.mobile}`;
        this.verifyOTP(data);
      } else {
        this.isFormInvalid = true;
      }
    }
  }

  signIn() {
    this.global.cLog(`In signin`);
    if (!this.signInData) {
      this.navCtrl.setRoot('LoginPage', { signInData: true });
    } else {
      this.navCtrl.setRoot('LoginPage', { signInData: false });
    }
  }

  termsAndCondition() {
    this.global.cLog(`in termsAndCondition`);
    this.navCtrl.push('TermsAndConditionPage');
  }

  privacyPolicy() {
    this.global.cLog(`in privacyPolicy`);
    this.navCtrl.push('PrivacyPolicyPage');
  }

  forgotPin() 
  {
    this.global.cLog(`in forgotPin`);
    this.navCtrl.push('ForgotPinPage');
  }

  

  filterEmoji(form: string, control: string, event: any) {

    let newValue = event.target.value;
    newValue = newValue.replace(this.global.emojiregix, "");
    //event.target.value = newValue;

    if (control == "mobile" || control == "otp" || control == "pin") {
      newValue = newValue.replace(this.global.numericregix, "");
    }
    
    
    if(control != ''){

      if(form == 'login'){ 
        this.loginForm.controls[control].setValue(newValue);
      }

      if(form == 'signup'){
        this.signUpForm.controls[control].setValue(newValue);
      }

      
    }else{
      event.target.value = newValue;
    }
    
  }


}