import { Component, ViewChild } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  Content,
  LoadingController,
  Loading,
  Events,
  AlertController
} from "ionic-angular";
import { GlobalProvider } from "../../providers/global/global";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Keyboard } from "@ionic-native/keyboard";
import { ThemeProvider } from "../../providers/theme/theme";
import { Camera, CameraOptions } from "@ionic-native/camera";
import { Diagnostic } from "@ionic-native/diagnostic";
import { DomSanitizer } from "@angular/platform-browser";
import { global } from "@angular/core/src/util";

@IonicPage()
@Component({
  selector: "page-profile",
  templateUrl: "profile.html"
})
export class ProfilePage {
  base64Image: string;
  professionalList: any;
  cityOriginList: any;
  cities: any;
  states: any;
  loader: Loading;
  noData: boolean;
  userProfile: any = {};
  modeOfCommunication: string[];
  profileForm: FormGroup;
  isDisabled: boolean = true;
  isFormInvalid: boolean = false;
  user_image: string = "";
  isPicturetaken: boolean = true;
  regExp = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;

  @ViewChild("ip") ip: any;
  @ViewChild(Content) content: Content;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public global: GlobalProvider,
    public fb: FormBuilder,
    public keyboard: Keyboard,
    public loadingController: LoadingController,
    private events: Events,
    private alrtCtrl: AlertController,
    private camera: Camera,
    public sanitizer: DomSanitizer,
    public theme: ThemeProvider,
    private diagnostic: Diagnostic
  ) {
    this.initForm();
    this.getProfessionalList();
    this.getCityOfOrigin();
    this.getStates();

    this.events.subscribe("user-updated-menu", data => {
      this.global.cLog(`in user-updated-menu`, data);

      if (data.user_image) {
        this.global.cLog(`in if`);
        this.user_image =
          this.global.image_base_path + "user/" + data.user_image;
        // this.user_image = this.global.sanatizeImage(false, 'user/' + data.user_image);
      }
      var img = localStorage.getItem("user_image");
      if (img != "" && img != undefined && img != null) {
        this.user_image = img;
      }
    });
    
  }

  initForm() {
    let EMAILPATTERN = new RegExp(
      /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/
    );
    let reg = new RegExp(/^[a-zA-Z ]*$/);
    let reg4 = new RegExp(/^[a-zA-Z0-9'\.\-\s\,]*$/);
    let reg1 = new RegExp(/^[a-zA-Z ]*$/);
    let reg2 = new RegExp(/^[A-Za-z0-9'\.\-\s\,]*$/);
    this.profileForm = this.fb.group({
      name: [
        JSON.parse(localStorage.getItem("user")).name,
        [Validators.required, Validators.pattern(reg)]
      ],
      email: [" ", [Validators.required, Validators.pattern(EMAILPATTERN)]],
      address: [" ", [Validators.required, Validators.pattern(reg4)]],
      modeOfCommunication: [["1"]],
      professionalService: ["1", [Validators.required]],
      city_of_origin: [" ", [Validators.required]],
      state_id: [" ", [Validators.required]],
      city_id: [" ", [Validators.required]],
      pincode: [
        " ",
        [
          Validators.required,
          Validators.maxLength(12),
          Validators.pattern(reg4)
        ]
      ]
    });

    this.userProfile["mobile_no"] = JSON.parse(
      localStorage.getItem("user")
    ).mobileno;
  }

  ionViewWillEnter() {
    

    console.log("ionViewDidLoad ProfilePage");

    this.getProfileData();
    this.isDisabled = false;
    //this.change();
    // this.keyboard.disableScroll(true);

    // this.keyboard.onKeyboardHide().subscribe(
    //   res => {
    //     this.global.cLog(`in onKeyboardHide`, res);
    //     this.removePadding();
    //   },
    //   err => {
    //     this.removePadding();
    //   }
    // );

    // this.keyboard.onKeyboardShow().subscribe(
    //   res => {
    //     this.global.cLog(`in onKeyboardHide`, res);
    //     this.removePadding();
    //   },
    //   err => {
    //     this.removePadding();
    //   }
    // );
  }

  edit() {
    this.global.cLog("edit clicked");
    this.isDisabled = !this.isDisabled;
  }

  submit() {

    if(this.isPicturetaken == true){
      this.saveProfileImage(this.base64Image).then( ok => {
        this.formsubmit()
      }, err => {
        this.global.showToast("Unable To Upload Image")
      });
    }else{
      this.formsubmit()
    }

    
  }

  formsubmit(){

    this.global.cLog("submit clicked", this.profileForm);

    if (this.profileForm.valid) {
      this.isFormInvalid = false;
      this.global.cLog("form is valid");
      this.updateProfileData();
    } else {
      this.global.cLog("form is invalid");
      this.isFormInvalid = true;

      if (this.profileForm.controls.name.invalid) {
        this.global.showToast(`please enter valid name`);
      } else if (this.profileForm.controls.email.invalid) {
        this.global.showToast(`please enter valid email`);
      } else if (this.profileForm.controls.address.invalid) {
        this.global.showToast(`please enter valid address`);
      } else if (this.profileForm.controls.professionalService.invalid) {
        this.global.showToast(`please chose valid professional service`);
      } else if (this.profileForm.controls.city_of_origin.invalid) {
        this.global.showToast(`please chose valid city`);
      } else if (this.profileForm.controls.state_id.invalid) {
        this.global.showToast(`please chose valid state`);
      } else if (this.profileForm.controls.city_id.invalid) {
        this.global.showToast(`please chose valid city`);
      } else if (this.profileForm.controls.pincode.invalid) {
        this.global.showToast(`please enter valid pincode`);
      } else {
        this.global.showToast(`invalid form try again`);
      }
    }

  }

  getProfileData() {
    // this.loader.present();
    this.global.showLoader(); 
    this.global
      .postRequest(this.global.base_path + "Login/Profile", {
        login_user_id: JSON.parse(localStorage.getItem("user")).id
      })
      .subscribe(
        res => {
          this.global.cLog(`getProfileData's user data is `, res);
          //this.loader.dismiss();
          this.global.hideLoader();
          if (res.success == "true") {
            this.noData = false;
            this.userProfile = res.userprofile;
            if (this.userProfile.state_id != "") {
              this.getStateCities(this.userProfile.state_id);
            }
            this.setFormData();
          } else {
            this.noData = true;
            this.global.cLog(`${res.error}`);
          }
        },
        err => {
          this.noData = true;
          this.global.hideLoader();
          this.global.cLog(`some error in getting user data`);
        }
      );
  }

  setFormData() {
    this.profileForm.controls["name"].setValue(this.userProfile.name);
    this.profileForm.controls["email"].setValue(this.userProfile.email);
    this.profileForm.controls["address"].setValue(this.userProfile.address);
    this.profileForm.controls["modeOfCommunication"].setValue(
      this.userProfile.mode_of_communication
    );
    this.profileForm.controls["professionalService"].setValue(
      this.userProfile.professional_service_id
    );
    this.profileForm.controls["city_of_origin"].setValue(
      this.userProfile.city_of_origin
    );
    this.profileForm.controls["city_id"].setValue(this.userProfile.city_id);
    this.profileForm.controls["state_id"].setValue(this.userProfile.state_id);
    this.profileForm.controls["pincode"].setValue(this.userProfile.pincode);

    if (this.userProfile.user_image) {
      this.global.cLog("asdfasdfasdfa" + this.userProfile.user_image);
      // this.user_image = this.global.sanatizeImage(false, 'user/' + this.userProfile.user_image);
      this.user_image =
        this.global.image_base_path + "user/" + this.userProfile.user_image;
      localStorage.setItem("user_image", this.user_image);
    } else {
      this.user_image = `'../assets/icon/thumnail-image.png'`;
      localStorage.setItem("user_image", "");
    }
    this.userProfile["mobile_no"] = JSON.parse(
      localStorage.getItem("user")
    ).mobileno;

    localStorage.setItem("profile-user", JSON.stringify(this.userProfile));
  }

  change() {
    this.global.cLog("this.ip is ", this.ip);
    let element = this.ip._elementRef.nativeElement;

    let textarea: HTMLElement = element.getElementsByTagName("textarea")[0];

    // set default style for textarea
    textarea.style.minHeight = "0";
    textarea.style.height = "0";

    // limit size to 96 pixels (6 lines of text)
    let scroll_height = textarea.scrollHeight;
    if (scroll_height > 96) scroll_height = 96;
    else scroll_height = 50;

    // apply new style
    element.style.height = scroll_height + "px";
    textarea.style.minHeight = scroll_height + "px";
    textarea.style.height = scroll_height + "px";
  }

  removePadding() {
    this.global.cLog(`in removePadding`);

    let contentNative: HTMLElement = this.content.getNativeElement();
    let foo: any = contentNative.getElementsByClassName("scroll-content");

    this.global.cLog(`'in keyboard hide res`, contentNative, foo);
    foo[0].style.paddingBottom = "0px";
  }

  updateProfileData() {
    let foo = this.navParams.get("data");

    let data = this.getUpdatedProfileDataToUpload();
    this.global.showLoader();
    // this.loader.present();
    this.global
      .postRequest(this.global.base_path + "Login/EditProfile", data)
      .subscribe(
        res => {
          this.global.hideLoader();
          // this.loader.dismiss();
          if (res.success == "true") {
            this.userProfile = res.user;
            this.setFormData();
            this.updateLocalStorage();
            this.global.showToast(`Successfully updated the profile`);
            if (foo && foo.fromLogin) {
              this.navCtrl.setRoot("MenuPage", { data: null });
            }
          } else {
            this.global.showToast(`${res.error}`);
          }
        },
        err => {
          // this.loader.dismiss();
          this.global.hideLoader();
          this.global.showToast(`Some error in updating profile`);
        }
      );
  }

  getUpdatedProfileDataToUpload() {
    let data = {
      login_user_id: JSON.parse(localStorage.getItem("user")).id,
      name: this.profileForm.controls["name"].value,
      email: this.profileForm.controls["email"].value,
      address: this.profileForm.controls["address"].value,
      mode_of_communication: this.profileForm.controls["modeOfCommunication"]
        .value,
      city_of_origin: this.profileForm.controls["city_of_origin"].value,
      // user_image: this.base64Image,
      state: this.profileForm.controls["state_id"].value,
      city: this.profileForm.controls["city_id"].value,
      country: this.theme.defaultTheme.default_country,
      pincode: this.profileForm.controls["pincode"].value,
      professional_service_id: this.profileForm.controls["professionalService"]
        .value
    };

    this.global.cLog(`data to be updated in profile api is`, data);

    return data;
  }

  updateLocalStorage() {
    //alert(this.userProfile.chat_available);
    let user = JSON.parse(localStorage.getItem("user"));
    user.name = this.profileForm.controls["name"].value;
    user.user_image = this.userProfile.user_image;
    user.email = this.userProfile.email;
    user.chat_available = this.userProfile.chat_available;

    localStorage.setItem("user", JSON.stringify(user));

    this.events.publish("user-updated", user);
  }

  getProfessionalList() {
    this.global
      .postRequest(this.global.base_path + "Login/ProfessionList", {})
      .subscribe(
        res => {
          this.global.cLog(`getPRofessional data`, res);
          if (res.success == "true") {
            this.professionalList = res.Profession;
          } else {
            this.global.cLog(`getPRofessional error`, res);
            this.global.showToast(res.error);
          }
        },
        err => {
          this.global.cLog(`getPRofessional error`, err);
        }
      );
  }

  getCityOfOrigin() {
    this.global
      .postRequest(this.global.base_path + "Login/City_Of_Origin_List", {})
      .subscribe(
        res => {
          this.global.cLog(`getCityOrigin data`, res);
          if (res.success == "true") {
            this.cityOriginList = res.Cities;
          } else {
            this.global.cLog(`getPRofessional error`, res);
            this.global.showToast(res.error);
          }
        },
        err => {
          this.global.cLog(`getPRofessional error`, err);
        }
      );
  }

  getStateCities(id) {
    if (id) {
      var st = id;
    } else {
      var st = this.profileForm.controls["state_id"].value;
    }
    this.global
      .postRequest(this.global.base_path + "Login/city", { state_id: st })
      .subscribe(
        res => {
          this.global.cLog(`getCityOrigin data`, res);
          if (res.success == "true") {
            this.cities = res.data;
          } else {
            this.global.cLog(`getPRofessional error`, res);
            //this.global.showToast(res.error);
          }
        },
        err => {
          this.global.cLog(`getPRofessional error`, err);
        }
      );
  }

  getStates() {
    this.global
      .postRequest(this.global.base_path + "Login/state", {
        country_id: this.theme.defaultTheme.default_country
      })
      .subscribe(
        res => {
          this.global.cLog(`getCityOrigin data`, res);
          if (res.success == "true") {
            this.states = res.data;
          } else {
            this.global.cLog(`getPRofessional error`, res);
            this.global.showToast(res.error);
          }
        },
        err => {
          this.global.cLog(`getPRofessional error`, err);
        }
      );
  }

  choosePhoto() {
    let alert = this.alrtCtrl.create({
      title: `Profile Picture`,
      buttons: [
        {
          text: "Gallery",
          handler: () => {
            this.global.cLog(`Gallery choosed`);
            this.handleCameraPermission(
              this.camera.PictureSourceType.PHOTOLIBRARY ||
                this.camera.PictureSourceType.SAVEDPHOTOALBUM
            );
          }
        },
        {
          text: "Camera",
          handler: () => {
            this.global.cLog(`Camera choosed`);
            this.handleCameraPermission(this.camera.PictureSourceType.CAMERA);
          }
        }
      ]
    });

    alert.present();
  }

  takePhoto(type: number) {
    const options: CameraOptions = {
      quality: 40,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: type,
      correctOrientation: true,
      saveToPhotoAlbum: true
    };

    this.camera.getPicture(options).then(
      imageData => {
        this.base64Image = "data:image/jpeg;base64," + imageData;
        this.user_image = this.base64Image;
        this.global.cLog(`got the image`, this.base64Image);
        this.isPicturetaken = true;
        // this.saveProfileImage(this.base64Image);
      },
      err => {
        // Handle error
        this.global.cLog(`Some error in taking picture`, err);
      }
    );
  }

  handleCameraPermission(type: number) {
    this.diagnostic
      .isCameraAuthorized()
      .then(res => {
        this.global.cLog(`Got the isCameraAuthorized res `, res);
        if (res) {
          this.takePhoto(type);
        } else {
          this.diagnostic
            .requestCameraAuthorization()
            .then(res => {
              this.global.cLog(`Got the requestCameraAuthorization res `, res);
              if (res) {
                this.takePhoto(type);
              } else {
                this.global.cLog(`App needs Camera Permission.`);
              }
            })
            .catch(err => {
              this.global.cLog(
                `Got the requestCameraAuthorization error `,
                err
              );
            });
        }
      })
      .catch(err => {
        this.global.cLog(`Got the isCameraAuthorized error`, err);
      });
  }

  saveProfileImage(image: string) {

    return new Promise( (resolve, reject) => {
      let data = {
        login_user_id: JSON.parse(localStorage.getItem("user")).id,
        user_image: image
      };
  
      this.global.showLoader();
      this.global
        .postRequest(this.global.base_path + "Login/SaveImage", data)
        .subscribe(
          res => {
            this.global.hideLoader();
            this.global.cLog(`saveprofile data`, res);
            //alert(JSON.stringify(data));
            //alert(JSON.stringify(res));
            if (res.success == "true") {
              this.global.showToast(`${res.message}`);
              this.user_image = this.global.image_base_path + "user/" + res.Image;
              // this.user_image = this.global.sanatizeImage(false, 'user/' + res.Image);
              let user = JSON.parse(localStorage.getItem("user"));
              user.user_image = res.Image;
              //user.user_image = this.global.image_base_path + 'user/' + res.Image;
              localStorage.setItem("user", JSON.stringify(user));
              this.user_image = this.global.image_base_path + "user/" + res.Image;
              //alert(this.user_image);
  
              this.events.publish("user-updated", user);
              this.isPicturetaken = false;
              resolve();
            } else {
              this.global.showToast(`${res.error}`);
              this.isPicturetaken = false;
              reject()
            }
          },
          err => {
            this.global.hideLoader();
            this.global.cLog(`Some error in save profile image`);
            this.isPicturetaken = false;
            reject();
          }
        );
    })
    
  }

  filterEmoji(control: string, event: any) {
    
    // let reg = new RegExp(/^[a-zA-Z ]*$/);
    // let reg22 = new RegExp(/[^a-zA-Z0-9'\.\-\@\_\s\, ]*$/);
    // let reg4 = new RegExp(/[^a-zA-Z0-9 -]/);
    // let reg5 = new RegExp(/[^a-zA-Z -]/);
    // let reg1 = new RegExp(/^[a-zA-Z ]*$/);
    // let reg2 = new RegExp(/[^A-Za-z0-9'\.\-\s\,]*$/);

    let newValue = event.target.value;
    newValue = newValue.replace(this.global.emojiregix, "");
    //event.target.value = newValue;

    if (control == "name") { 
      newValue = newValue.replace(this.global.alpharegix, "");
    }
    if (control == "address") {
      newValue = newValue.replace(this.global.aplhanumericdotcommadashregix, "");
    }
    if (control == "email") {
      newValue = newValue.replace(this.global.emailregix, "");
    }

    if (control == "pincode") {
      newValue = newValue.replace(this.global.alphanumeric, "");
      newValue = newValue.substring(0, 11);
    }
    //event.target.value = newValue;
    this.profileForm.controls[control].setValue(newValue);

    
  }

  removePadding1() {
    let control = "email";
    let reg1 = new RegExp(
      /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/
    );
    if (!reg1.test(this.profileForm.controls[control].value)) {
      console.log("no");
      this.profileForm.controls[control].setValue(
        this.profileForm.controls[control].value.slice(0, -1)
      );
      // this.profileForm.controls[control].setValue(this.profileForm.controls[control].value.replace(/([\uE000-\uF8FF]|\uD83C[\uDC00-\uDFFF]|\uD83D[\uDC00-\uDFFF]|[\u2694-\u2697]|\uD83E[\uDD10-\uDD5D])/g, ''));
    } else {
      console.log("yes");
    }
  }
}
