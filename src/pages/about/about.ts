import { GlobalProvider } from './../../providers/global/global';
import { Component, ViewChild, ElementRef } from '@angular/core';
import { IonicPage, NavController, NavParams,LoadingController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-about',
  templateUrl: 'about.html',
})
export class AboutPage {

  noData: boolean;
  loader:any;

  @ViewChild('pTag') pTag: ElementRef;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private global: GlobalProvider, public loadingCtrl:LoadingController
  ) {
    this.getData();
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AboutPage');
  }

  getData() 
  {
    this.showLoaders();
    this.global.postRequest(this.global.base_path + 'Login/Aboutus', {})
      .subscribe(res => {
        this.global.cLog(`contact data is `, res);
        this.hideLoaders();
        if (res.success == 'true') {
          this.noData = false;
          this.pTag.nativeElement.innerHTML = res.aboutus;
        } else {
          this.noData = true;
          //this.global.showToast(`No data found`);
        }
      }, err => {
        this.hideLoaders();
        this.noData = true;
        this.global.cLog(`some error in contact data is `, err);
      });
  }

  showLoaders()
  {
     this.loader = this.loadingCtrl.create({
            content: 'Please wait..'
         });
      this.loader.present();
  }
  hideLoaders()
  {
    this.loader.dismiss();
  }
}