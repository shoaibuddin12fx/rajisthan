import { GlobalProvider } from './../../providers/global/global';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,ModalController,Events,LoadingController } from 'ionic-angular';
import { ThemeProvider } from '../../providers/theme/theme';
import {SidebarService} from '../../services/sidebar.service';

@IonicPage()
@Component({
  selector: 'page-event',
  templateUrl: 'event.html',
})
export class EventPage {
  pet: string = "kittens";
  noData: boolean;
  noData1: boolean;
  eventDummydata: any[] = [];
  oldEvents:any[]= [];
  loader:any;
  userDetails:any;

  constructor(public navCtrl: NavController, public navParams:NavParams,public global:GlobalProvider,public theme:ThemeProvider,private modal: ModalController,
    public loadingCtrl: LoadingController,public events: Events ,public sidebarService: SidebarService) 
  {
     this.userDetails = JSON.parse(localStorage.getItem('user'));

     //console.log('================');
     //console.log(this.navCtrl);
     //var data= {user_id:114};
     //this.events.publish('approved',data);

     this.events.subscribe('approved',(data)=>{ //alert("yes");
     let user = JSON.parse(localStorage.getItem('user'));
     user.status=2;
     user.status_id=2;
     this.userDetails.status_id= 2;
     localStorage.setItem('user', JSON.stringify(user));
     //this.getEventList();

      this.toggleSidebar({userid:data.user_id,status:2});
     });
  }

  toggleSidebar(data) 
  {
    console.log("in toggle");
    console.log(data);
    this.sidebarService.toggleSidebarVisibilty(data);
  }

  ionViewDidLoad() 
  {
    console.log('ionViewDidLoad EventPage');
    //this.getEventList();
  }

  ionViewDidEnter()
  {
     this.getEventList();
  }

  getEventList() 
  {
    //this.showLoaders();
    this.global.postRequest(`${this.global.base_path}Login/EventList`, {}).subscribe(res => {
       // this.hideLoaders();
        this.getOldEvents();
        this.global.cLog(`event list response`, res);
        if (res.success == 'true') 
        {
            // this.global.showToast(`${res.message}`);
            this.noData = false;
            this.eventDummydata = res.event;
            this.eventDummydata.forEach((res, i) => {
                this.eventDummydata[i].event_image = this.global.sanatizeImage(true, `${this.eventDummydata[i].event_image}`);
                this.eventDummydata[i].type = 'new';
            });
        }
        else 
        {
              this.noData = true;
              //this.global.showToast(`${res.error}`);
        }
      }, err => {
          //this.hideLoaders();
          this.getOldEvents();
          this.noData = true;
          this.global.cLog(`event list error`, err);
      });
  }

  showLoaders()
  {
     this.loader = this.loadingCtrl.create({
                    content: 'Please wait...'
                 });
     this.loader.present();
  }
  hideLoaders()
  {
    this.loader.dismiss();
  }

  getOldEvents() 
  {
    //this.showLoaders();
    this.global.postRequest(`${this.global.base_path}Login/OldEventList`, {}).subscribe(res => {
           //this.hideLoaders();
          this.global.cLog(`event list response`, res);
          if (res.success == 'true') {
            // this.global.showToast(`${res.message}`);
            this.noData1 = false;
            this.oldEvents = res.event;
            this.oldEvents.forEach((res, i) => {
              this.oldEvents[i].event_image = this.global.sanatizeImage(true, `${this.oldEvents[i].event_image}`);
              this.oldEvents[i].showDiv = 'false';
              this.oldEvents[i].type = 'old';
              this.oldEvents[i].event ='';
            });
          } else {
            this.noData1 = true;
            //this.global.showToast(`${res.error}`);
          }
        }, err => {
           //this.hideLoaders();
          this.noData1 = true;
          this.global.cLog(`event list error`, err);
         
        });
  }

  getcomment1(i)
  {
      this.showLoaders();
      this.global.postRequest(`${this.global.base_path}Login/GetEventFeedback`, {user_id:localStorage.getItem('user_id'),event_id:this.oldEvents[i].id}).subscribe(res => {
          this.hideLoaders();
          if(this.oldEvents[i].showDiv=='true'){ this.oldEvents[i].showDiv='false'} else { this.oldEvents[i].showDiv='true';}
          if (res.success == 'true') 
          {
             this.oldEvents[i].feedback = res.feedback;
             //this.global.showToast(`${res.message}`);
          }
      }, err => {
           this.hideLoaders();
      });
  }

  getcomment(i) 
  {  
    //this.showLoaders();
    //this.global.postRequest(`${this.global.base_path}Login/GetEventFeedback`, {user_id:localStorage.getItem('user_id'),event_id:this.oldEvents[i].id}).subscribe(res => {
          //this.hideLoaders();
          //if(this.oldEvents[i].showDiv=='true'){ this.oldEvents[i].showDiv='false'} else { this.oldEvents[i].showDiv='true';}
           
            //this.oldEvents[i].feedback = res.feedback;
            let performance = this.modal.create(
              'CommentPage',
              {
                //"data": this.oldEvents[i].feedback,
                "user_id":localStorage.getItem('user_id'),
                "event_id":this.oldEvents[i].id,
              },
              { cssClass: 'performance' }
            );
            performance.present();
            performance.onDidDismiss(data => {
              this.global.cLog(`modal data`, data);
            });
           
      //}, err => {
        //   this.hideLoaders();
    //});
  }





  saveevent(i)
  {
    if(this.oldEvents[i].feedback!='' && this.oldEvents[i].feedback!=undefined && this.oldEvents[i].feedback!=null)
    {
      this.showLoaders();
      this.global.postRequest(`${this.global.base_path}Login/UpdateEventFeedback`, {user_id:localStorage.getItem('user_id'),event_id:this.oldEvents[i].id,feedback_text:this.oldEvents[i].feedback}).subscribe(res => {
           this.hideLoaders();
          if (res.success == 'true') 
          {
             this.oldEvents[i].showDiv = 'false';
             this.global.showToast(`${res.message}`);
          }
      }, err => {
           this.hideLoaders();
      });
    }
  }

  openEvent(i: number) 
  {
    this.global.cLog('in openEvent() with data ', this.eventDummydata[i]);
    this.navCtrl.push('CommunityNamePage', { data: this.eventDummydata[i] });
  }

  openOldEvent(i: number) 
  {
    this.global.cLog('in openEvent() with data ', this.oldEvents[i]);
    this.navCtrl.push('CommunityNamePage', { data: this.oldEvents[i] });
  }

  filterEmoji(event: any) 
  {
    let newValue = event.target.value;
    newValue = newValue.replace(this.global.emojiregix, '');
    event.target.value = newValue;
  }
  
}
