var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
var SidebarService = /** @class */ (function () {
    function SidebarService() {
        var _this = this;
        this.menuPage = ['userid', 'status'];
        this.sidebarVisibilityChange = new Subject();
        this.sidebarVisibilityChange.subscribe(function (value) {
            _this.menuPage = value;
        });
    }
    SidebarService.prototype.toggleSidebarVisibilty = function (data) {
        this.sidebarVisibilityChange.next(data);
    };
    SidebarService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [])
    ], SidebarService);
    return SidebarService;
}());
export { SidebarService };
//# sourceMappingURL=sidebar.service.js.map